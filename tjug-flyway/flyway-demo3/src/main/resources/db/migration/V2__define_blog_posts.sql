CREATE TABLE demo2.blog_posts(
	user_account_fk  INTEGER REFERENCES demo2.user_accounts(id),
	title VARCHAR(256),
	body TEXT
);