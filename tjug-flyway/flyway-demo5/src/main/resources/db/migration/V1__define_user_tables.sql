/*
 *  Define the tables for storing users and their roles
 */

CREATE TABLE demo5.user_accounts(
    id       SERIAL PRIMARY KEY, 
    name     VARCHAR(256),
	email    VARCHAR(320),
    password VARCHAR(50)
);

CREATE TABLE demo5.user_roles(
	user_account_fk  INTEGER REFERENCES demo5.user_accounts(id),
	role_name VARCHAR(64)
);