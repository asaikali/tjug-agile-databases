package com.example.flyway;
import org.springframework.stereotype.Repository;


@Repository
public class SomeRepository {
	public void getSomething()
	{
		System.out.println("SomeRepository.getSomething()");
	}
}
