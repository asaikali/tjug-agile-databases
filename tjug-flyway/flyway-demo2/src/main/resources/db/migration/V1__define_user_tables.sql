/*
 *  Define the tables for storing users and their roles
 */

CREATE TABLE demo2.user_accounts(
    id       INTEGER PRIMARY KEY, 
    name     VARCHAR(256),
	email    VARCHAR(320),
    password VARCHAR(50)
);

CREATE TABLE demo2.user_roles(
	user_account_fk  INTEGER REFERENCES demo2.user_accounts(id),
	role_name VARCHAR(64)
);