package com.example.demo;


import javax.sql.DataSource;

import org.postgresql.ds.PGSimpleDataSource;

import com.googlecode.flyway.core.Flyway;

public class FlywayDemo4 
{
	public static void main(String[] args) 
	{
		Flyway flyway = new Flyway();
		flyway.setDataSource(getDataSource());
		flyway.setSchemas("demo3");
		flyway.migrate();
		
		System.out.println("+++ DEMO END");
	}

	private static DataSource getDataSource() {
		PGSimpleDataSource dataSource = new PGSimpleDataSource();

		dataSource.setServerName("localhost");
		dataSource.setPortNumber(5432);
		dataSource.setDatabaseName("flyway_demos");
		dataSource.setUser("flyway");
		dataSource.setPassword("flyway");
		return dataSource;
	}
}
